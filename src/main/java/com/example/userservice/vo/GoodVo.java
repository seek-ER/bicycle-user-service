package com.example.userservice.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GoodVo {
  private Long id;
  private Long bicycleId;
  private Integer amount;
  private Double unitGoodPrice;
  private Double totalGoodPrice;
  private Long order;
}
