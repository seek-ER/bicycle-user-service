package com.example.userservice.vo;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderVo {
  private Long id;
  private Long userId;
  private List<GoodVo> goods;
  private Double totalOrderPrice;
}
