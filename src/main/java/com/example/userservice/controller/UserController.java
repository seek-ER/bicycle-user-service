package com.example.userservice.controller;

import com.example.userservice.dto.UserDto;
import com.example.userservice.entity.User;
import com.example.userservice.service.UserService;
import com.example.userservice.vo.OrderVo;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {
  private UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public User addUser(@RequestBody UserDto userDto) {
    return userService.addUser(userDto);
  }

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public User getUser(@RequestParam("id") Long userId) {
    return userService.getUser(userId);
  }

  @GetMapping("/orders")
  @ResponseStatus(HttpStatus.OK)
  public List<OrderVo> getOrders(@RequestParam("id") Long userId) throws ExecutionException, InterruptedException {
    return userService.getOrders(userId).get();
  }

  @DeleteMapping
  @ResponseStatus(HttpStatus.OK)
  public void deleteUser(@RequestParam("id") Long userId) {
    userService.deleteUser(userId);
  }

  @PutMapping
  @ResponseStatus(HttpStatus.ACCEPTED)
  public User alterUser(@RequestParam("id") Long userId, @RequestBody UserDto userDto) {
    return userService.alterUser(userId, userDto);
  }
}
