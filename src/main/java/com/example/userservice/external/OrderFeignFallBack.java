package com.example.userservice.external;

import com.example.userservice.vo.OrderVo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class OrderFeignFallBack implements OrderClient {
  @Override
  public List<OrderVo> getOrders(Long userId) {
    return new ArrayList<>();
  }
}
