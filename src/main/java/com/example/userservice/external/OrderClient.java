package com.example.userservice.external;

import com.example.userservice.vo.OrderVo;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
    name = "order-service",fallback = OrderFeignFallBack.class
)
public interface OrderClient {
  @GetMapping(value = "/orders/userOrders")
  List<OrderVo> getOrders(@RequestParam("user_id") Long userId);
}
