package com.example.userservice.service;

import com.example.userservice.dto.UserDto;
import com.example.userservice.entity.User;
import com.example.userservice.external.OrderClient;
import com.example.userservice.repository.UserRepository;
import com.example.userservice.vo.OrderVo;
import java.util.List;
import java.util.concurrent.Future;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

@Service
public class UserService {
  private UserRepository userRepository;
  private OrderClient orderClient;

  public UserService(UserRepository userRepository,
                     OrderClient orderClient) {
    this.userRepository = userRepository;
    this.orderClient = orderClient;
  }

  public User addUser(UserDto userDto) {
    User user = User.builder()
        .name(userDto.getName())
        .phone(userDto.getPhone())
        .build();
    return userRepository.save(user);
  }

  public User getUser(Long userId) {
    return userRepository.findById(userId).get();
  }

  public void deleteUser(Long userId) {
    userRepository.deleteById(userId);
  }

  public User alterUser(Long userId, UserDto userDto) {
    User user = userRepository.findById(userId).get();
    if (null != userDto.getName()) {
      user.setName(userDto.getName());
    }
    if (null != userDto.getPhone()) {
      user.setPhone(userDto.getPhone());
    }
    return userRepository.save(user);
  }

  @Async
  public Future<List<OrderVo>> getOrders(Long userId) {
    System.out.println(Thread.currentThread().getName());
    System.out.println(RequestContextHolder.getRequestAttributes());
    return new AsyncResult<>(orderClient.getOrders(userId));
  }
}
