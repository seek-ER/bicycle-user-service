package com.example.userservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.core.task.TaskDecorator;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;

@Configuration
@EnableAsync
public class AsyncConfiguration extends AsyncSupportConfigurer {
  @Bean
  public ThreadPoolTaskExecutor asyncExecutor() {
    ThreadPoolTaskExecutor threadPool = new ThreadPoolTaskExecutor();
    threadPool.setCorePoolSize(3);
    threadPool.setMaxPoolSize(3);
    threadPool.setQueueCapacity(500);
    threadPool.setWaitForTasksToCompleteOnShutdown(true);
    threadPool.setAwaitTerminationSeconds(60 * 15);
    threadPool.setTaskDecorator(new RequestDecorator());
    threadPool.initialize();
    return threadPool;
  }

  @Override
  protected AsyncTaskExecutor getTaskExecutor() {
    return asyncExecutor();
  }

  private class RequestDecorator implements TaskDecorator {
    @Override
    public Runnable decorate(Runnable runnable) {
      final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
      return () -> {
        try {
          RequestContextHolder.setRequestAttributes(requestAttributes);
          runnable.run();
        } finally {
          RequestContextHolder.resetRequestAttributes();
        }
      };
    }
  }

}
