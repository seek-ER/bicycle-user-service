create table user
(
    id                  BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'USERID',
    name                VARCHAR(64) NOT NULL COMMENT '用户名',
    phone               VARCHAR(64) COMMENT '手机号'
);